# REAMDE

## Aquire container

### Pull image from docker hub
```bash
docker pull beryj7/dicod-docker && \
    docker run -d --rm --name="dicod" -p2628:2628 beryj7/dicod-docker
```

### Build and run container instance

N.B. You may need to have git-lfs installed to properly clone this repository.
Without git-lfs, wikitionary won't be installed. To check whether the compressed
wiktionary archive has been successfully downloaded look at the size of
./wikt-en-ALL-2018-05-15-dictd.7z; it should be around 104MB. If it's too small,
install git-lfs and run `git lfs checkout`.

The more involved method: ```bash docker build . -t beryj7/dicod-docker:latest
&& \ docker run --name="dicod" --rm -d -p2628:2628 beryj7/dicod-docker:latest
```

## Query

### By attaching to DICT server container
The preferred query method.
```bash
docker exec dicod dico [<query-opts>] <word>
```

## systemd service

A service unit for dicod-docker looks like:

```
[Unit] Description=Dockerized GNU Dico DICT server

[Service] ExecStart=docker run --name="dicod" --rm -d -2628:2628
beryj7/dicod-docker:latest

[Install]
WantedBy=multi-user.target

```

### Via exposed port on host system
Assuming that dico or dict is installed on the host, an instance is running,
and port 2628 is exposed, the following query should work:
```bash
dico -p 2628 <word>
```

[dico package]: ftp://download.gnu.org.ua/pub/release/dico/dico-2.4.tar.gz

### Dico wrapper for [neo]vim

For a nifty vim client see [dico-vim](https://gitlab.com/metaporia/dico-vim).
